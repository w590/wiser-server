using Wiser.Models;

namespace Wiser.Auth.JwtTokens
{
    public interface IJwtTokenFactory
    {
        string Create(User user);
    }
}