using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security;
using System.Security.Claims;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Wiser.Models;

namespace Wiser.Auth.JwtTokens
{
    public class JwtTokenFactory : IJwtTokenFactory
    {
        private static readonly JwtSecurityTokenHandler Handler = new();
        private readonly SigningCredentials _credentials;
        private readonly AuthOptions _options;

        public JwtTokenFactory(IOptions<AuthOptions> options)
        {
            _options = options.Value;
            _credentials =
                new SigningCredentials(
                    _options.SecurityKey ?? throw new NullReferenceException(nameof(_options.SecurityKey)),
                    SecurityAlgorithms.HmacSha256Signature);
        }

        public string Create(User user)
        {
            var now = DateTime.UtcNow;
            var expires = now.Add(TimeSpan.FromSeconds(_options.TokenLifetime ??
                                                       throw new SecurityException(
                                                           $"{nameof(_options.TokenLifetime)} must be configured in ${nameof(AuthOptions)}")));
            var token = new JwtSecurityToken(_options.Issuer, _options.Audience,
                new Claim[] {new(ClaimTypes.Name, user.Id.ToString())}, signingCredentials: _credentials,
                notBefore: now, expires: expires);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}