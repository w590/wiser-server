using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Wiser.Auth.RefreshTokens
{
    [Table("RefreshTokens")]
    public class RefreshToken
    {
        public long Id { get; set; }
        public string Token { get; set; }
        public DateTime Expires { get; set; }
        public DateTime Created { get; set; }
        public bool IsExpire => DateTime.UtcNow >= Expires;
        public DateTime? Revoked { get; set; }
        public bool IsActive => Revoked == null && !IsExpire;
    }
}