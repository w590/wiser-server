using System.Threading.Tasks;

namespace Wiser.Auth.Registration
{
    public interface IRegistrationService
    {
        Task<RegistrationResponse> RegisterAsync(RegistrationRequest request);
    }
}