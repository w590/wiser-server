namespace Wiser.Auth.Registration
{
    public class RegistrationRequest
    {
        public string Password { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}