using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Wiser.Auth.JwtTokens;
using Wiser.Auth.RefreshTokens;
using Wiser.Models;

namespace Wiser.Auth.Registration
{
    public class RegistrationService : IRegistrationService
    {
        private readonly DbContext _context;
        private readonly IJwtTokenFactory _jwtTokenFactory;
        private readonly IRefreshTokenFactory _refreshTokenFactory;

        public RegistrationService(DbContext context, IJwtTokenFactory jwtTokenFactory,
            IRefreshTokenFactory refreshTokenFactory)
        {
            _context = context;
            _jwtTokenFactory = jwtTokenFactory;
            _refreshTokenFactory = refreshTokenFactory;
        }

        public async Task<RegistrationResponse> RegisterAsync(RegistrationRequest request)
        {
            if (await _context.Users.AnyAsync(x => x.Email.ToLower() == request.Email.ToLower()))
                return new RegistrationResponse
                {
                    Status = RegistrationResponse.RegistrationStatus.UserAlreadyExist,
                };

            var user = new User
            {
                Email = request.Email,
                Name = request.Name,
                Password = request.Password,
            };
            var refreshToken = _refreshTokenFactory.Create(TimeSpan.FromDays(1));
            user.RefreshTokens.Add(refreshToken);
            await _context.AddAsync(user);
            await _context.SaveChangesAsync();
            return new RegistrationResponse
            {
                Status = RegistrationResponse.RegistrationStatus.Ok,
                AuthenticationResponse = new AuthenticationResponse
                {
                    Id = user.Id,
                    Token = _jwtTokenFactory.Create(user),
                    RefreshToken = refreshToken.Token,
                },
            };
        }
    }
}