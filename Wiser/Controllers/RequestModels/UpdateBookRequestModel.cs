namespace Wiser.Controllers.RequestModels
{
    public class UpdateBookRequestModel
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public int? CurrentPage { get; set; }
        public int? PagesCount { get; set; }
        public bool? IsCompleted { get; set; }
    }
}