using Microsoft.EntityFrameworkCore;
using Wiser.Auth.RefreshTokens;
using Wiser.Models;

namespace Wiser
{
    public class DbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public DbSet<User> Users { get; set; } = null!;
        public DbSet<Book> Books { get; set; } = null!;
        public DbSet<RefreshToken> RefreshTokens { get; set; }

        public DbContext(DbContextOptions<DbContext> options) : base(options)
        {
        }
    }
}