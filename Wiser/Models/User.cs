using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Wiser.Auth.RefreshTokens;

namespace Wiser.Models
{
    [Table("Users")]
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public List<RefreshToken> RefreshTokens { get; set; } = new();
        public List<Book> Books { get; set; } = new();
        public int CurrentBookId { get; set; } = -1;
    }
}